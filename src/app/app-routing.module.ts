import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'activity' },
  { path: 'activity', loadChildren: () => import('./pages/masterpage/masterpage.module').then(m => m.MasterpageModule) },
  { path: 'user', loadChildren: () => import('./pages/user/user-routing.module').then(m => m.UserRoutingModule) }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    scrollPositionRestoration: 'enabled'
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
