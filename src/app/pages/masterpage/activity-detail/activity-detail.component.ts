import { Component, OnInit, HostListener } from '@angular/core';

@Component({
  selector: 'app-activity-detail',
  templateUrl: './activity-detail.component.html',
  styleUrls: ['./activity-detail.component.scss']
})
export class ActivityDetailComponent implements OnInit {

  constructor() { }

  @HostListener('window:scroll')
  onScroll() {
    var pageTop = window.pageYOffset;
    var windowHeight = window.innerHeight;
    var pageHeight = document.documentElement.scrollHeight;
    var fooetHeight = document.getElementById('global-footer').offsetHeight;

    var stickyfoot = document.getElementById('stickyfoot');

    if (pageTop > pageHeight - (windowHeight + fooetHeight)) {
      return 'stick';
    } else {
      return 'float';
    }
  }

  ngOnInit(): void {
  }


}
