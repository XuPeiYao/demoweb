import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomepageComponent } from './homepage/homepage.component';
import { Routes, RouterModule } from '@angular/router';
import { ActivityDetailComponent } from './activity-detail/activity-detail.component';
import { CreateActivityComponent } from './create-activity/create-activity.component';

const routes: Routes = [{
  path: '',
  pathMatch: 'full',
  redirectTo: 'find'
},
{
  path: 'find',
  component: HomepageComponent
},
{
  path: 'create',
  component: CreateActivityComponent
},
{
  path: 'find/:id',
  component: ActivityDetailComponent
},

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MasterpageRoutingModule { }
