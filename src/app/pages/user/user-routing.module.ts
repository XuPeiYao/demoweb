import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MyActivityComponent } from './my-activity/my-activity.component';


const routes: Routes = [{
  path: 'my',
  component: MyActivityComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
