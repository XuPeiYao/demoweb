import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserRoutingModule } from './user-routing.module';
import { MyActivityComponent } from './my-activity/my-activity.component';


@NgModule({
  declarations: [MyActivityComponent],
  imports: [
    CommonModule,
    UserRoutingModule
  ]
})
export class UserModule { }
